import Vue from 'vue'
import Vuex from 'vuex'
import router from './router'
import axios from 'axios';
Vue.use(Vuex)  

export default new Vuex.Store({
  state: {
    people_list:[
      {
         first_name: 'coddy', 
          last_name: 'duder',
          email: 'cuddydudder@causelabs.com',
          age: 38,
          secret: 'VXNlIHRoaXMgc2VjcmV0IHBocmFzZSBzb21ld2hlcmUgaW4geW91ciBjb2RlJ3MgY29tbWVudHM=',

          },
          {
          first_name: 'ladee', 
          last_name: 'linter',
          email: 'lindaladee@causelabs.com', 
          age: 99,
          secret: 'cmVzb3VyY2UgdmlvbGF0aW9u',
          },
    ], 
  },   
  getters:{
    get_people_list(state){
      return state.people_list;
    }
  },
  mutations: { 
    add_people_list(state,people){

      state.people_list.push(people);
      console.log(state.people_list)
      router.push('/');  
    },
    delete_people_list(state,index){
      state.people_list.splice(index, 1)  
    }       

  }, 
  actions: {
   Async_add_people_list({commit},people){
     return new Promise((resolve,reject)=>{
      setTimeout(() => { 
        console.log(JSON.stringify({
          data:[
            {
              first_name:people.first_name,
              last_name:people.last_name, 
              age:people.age,
              email:people.email,
              secret:people.secret,  
            }
          ]
        }));
        axios.post('http://192.241.152.33/api/user/add',{
          data:[
            {
              first_name:people.first_name,
              last_name:people.last_name, 
              age:people.age,
              email:people.email,
              secret:people.secret,
            }
          ] 
        }).then((data)=>   {
          
          commit('add_people_list',people)
          console.log(data.data[0])
        }
         )  
        
        resolve()
      }, 1000)
   }
     )
  },
  Async_delete_people_list({commit},people){
    return new Promise((resolve,reject)=>{
      setTimeout(()=>{
        commit('delete_people_list',people)
        resolve()
      },1000)
    })
  }
  }
})
