import Vue from 'vue'
import Router from 'vue-router'
import  peopleList  from './components/people-list.vue';

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [ 
    {  
      path: '/',
      name: 'peopleList',
      component: () => import('./components/people-list.vue') 
    },
    {
      path: '/add_people', 
      name: 'add_people',
      component: () => import('./components/add_people.vue')
    }
    
  ]
})
